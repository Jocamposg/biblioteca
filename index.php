<?php
require_once './shared/sessions.php';
$title = 'Biblioteca - Inicio';
require_once './shared/header.php';

$urlInicio = './';
$validador = './validador.php';

	if(!isset($_SESSION['estado']) || empty($_SESSION['estado']))
	{
		$estado = 'Sign in';
		require_once './shared/Navbar.php';
	}
	else
	{
		$estado = 'Sign out';
		require_once './shared/Navbar.php';
	}
?>
	<div class="row" style="width: 101%; height: 130%;">
		<div class="col-1">
		</div>

		<?php
			if(isset($_SESSION['estado']) || !empty($_SESSION['estado']))
			{
		?>
		<div class="col-md-10">
			<div class="container" style="text-align: center; margin-top: 155px;">
				<a class="btn btn-light" href="./lending/create.php">
        	<i class="far fa-registered fa-7x fa-spin"></i>
        	<br><br>
        	Registar prestamo
      	</a>
      	<a class="btn btn-light" style="margin-left: 20%;" href="./lending/books.php">
        	<i class="fas fa-book fa-7x fa-spin"></i>
        	<br><br>
        	Prestamos
      	</a>
			</div>
		</div>
		<?php
			}
			else
			{
		?>
		<div class="col-md-10">
			<div class="container" style="text-align: center; margin-top: 155px; margin-bottom: 135px;">
				<h3 class="text-center">Bienvenidos</h3>
			</div>
		</div>
		<?php
			}
		?>
	  <div class="col-md-1">
		</div>
	</div>

<?php require_once './shared/footer.php'?>

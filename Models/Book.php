<?php
namespace Models {

  class Book
  {
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function find($id)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE id = $1', [$id])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function findTittle($titulo)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE titulo = $1', [$titulo])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function findCode($codigo)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE codigo = $1', [$codigo])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function findNum($numm, $numM)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE cant_prestamo >= $1 OR cant_prestamo <= $2', [$numm, $numM])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function all()
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro ORDER BY id ASC');
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function allLending()
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.prestamo ORDER BY id ASC');
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function allLend()
    {
      try
      {
        return $this->connection->runQuery('SELECT b.id, b.titulo, b.codigo, b.estado, b.cant_prestamo, b.tipo, l.fecha_salida, l.fecha_entrega
    			FROM biblioteca.prestamo l
  					INNER JOIN biblioteca.libro b ON b.id = l.id_libro
              WHERE b.estado = $1', ['true']);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function allLendT($titulo)
    {
      try
      {
        return $this->connection->runQuery('SELECT b.id, b.titulo, b.codigo, b.estado, b.cant_prestamo, b.tipo, l.fecha_salida, l.fecha_entrega
    			FROM biblioteca.prestamo l
  					INNER JOIN biblioteca.libro b ON b.id = l.id_libro
              WHERE b.titulo = $1', [$titulo]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function allLendC($codigo)
    {
      try
      {
        return $this->connection->runQuery('SELECT b.id, b.titulo, b.codigo, b.estado, b.cant_prestamo, b.tipo, l.fecha_salida, l.fecha_entrega
    			FROM biblioteca.prestamo l
  					INNER JOIN biblioteca.libro b ON b.id = l.id_libro
              WHERE b.codigo = $1', [$codigo]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function allLendE($estadoL)
    {
      try
      {
        return $this->connection->runQuery('SELECT b.id, b.titulo, b.codigo, b.estado, b.cant_prestamo, b.tipo, l.fecha_salida, l.fecha_entrega
    			FROM biblioteca.prestamo l
  					INNER JOIN biblioteca.libro b ON b.id = l.id_libro
              WHERE b.tipo = $1', [$estadoL]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function allLendCant($cant_ini, $cant_fin)
    {
      try
      {
        return $this->connection->runQuery('SELECT b.id, b.titulo, b.codigo, b.estado, b.cant_prestamo, b.tipo, l.fecha_salida, l.fecha_entrega
    			FROM biblioteca.prestamo l
  					INNER JOIN biblioteca.libro b ON b.id = l.id_libro
              WHERE b.cant_prestamo >= $1 AND b.cant_prestamo <= $2', [$cant_ini,$cant_fin]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function allLendFecha($fecha_ini, $fecha_fin)
    {
      try
      {
        return $this->connection->runQuery('SELECT b.id, b.titulo, b.codigo, b.estado, b.cant_prestamo, b.tipo, l.fecha_salida, l.fecha_entrega
    			FROM biblioteca.prestamo l
  					INNER JOIN biblioteca.libro b ON b.id = l.id_libro
              WHERE l.fecha_salida >= $1 AND l.fecha_salida <= $2', [$fecha_ini,$fecha_fin]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function allActive()
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE tipo = $1 ORDER BY id ASC', ["D"]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function create($titulo, $codigo, $tipo, $estado)
    {
      try
      {
        $this->connection->runStatement('INSERT INTO biblioteca.libro(titulo, codigo, tipo, estado) VALUES ($1, $2, $3, $4)', [$titulo, $codigo, $tipo, $estado]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function updatePrestamo($id, $cantidad, $tipo)
    {
      try
      {
        $this->connection->runStatement('UPDATE biblioteca.libro SET cant_prestamo = $2, tipo = $3 WHERE id = $1', [$id, $cantidad, $tipo]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function update($id, $titulo, $codigo, $tipo, $estado)
    {
      try
      {
        $this->connection->runStatement('UPDATE biblioteca.libro SET titulo=$2, codigo=$3, tipo=$4, estado=$5
            WHERE id=$1', [$id, $titulo, $codigo, $tipo, $estado]);
      } catch (PDOException $e) {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function delete($id, $motivo)
    {
      try
      {
        $this->connection->runStatement('UPDATE biblioteca.libro SET estado = $2, tipo = $3, motivo = $4 WHERE id = $1', [$id, 'false', 'A', $motivo]);
      } catch (PDOException $e) {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }
  }
}
?>

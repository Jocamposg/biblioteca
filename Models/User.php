<?php
namespace Models {

  class User
  {
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function login($username, $password)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.usuario WHERE usuario = $1 AND contrasena = md5($2)', [$username, $password])[0];
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function find($id)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.usuario WHERE id = $1', [$id])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function all()
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.usuario ORDER BY id ASC');
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function create($nombre, $apellido, $direccion, $fecha_nacimiento, $usuario, $contrasena, $correo, $estado)
    {
      try
      {
        $this->connection->runStatement('INSERT INTO biblioteca.usuario(nombre, apellido, direccion, fecha_nacimiento, usuario, contrasena, correo, estado) VALUES ($1, $2, $3, $4, $5, md5($6), $7, $8)',[$nombre, $apellido, $direccion, $fecha_nacimiento, $usuario, $contrasena, $correo, $estado]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function update($id, $nombre, $apellido, $direccion, $fecha_nacimiento, $usuario, $contrasena, $correo, $estado)
    {
      try
      {
        if($contrasena != '')
        {
          $this->connection->runStatement('UPDATE biblioteca.usuario SET nombre=$2, apellido=$3, direccion=$4, fecha_nacimiento=$5, usuario=$6, contrasena=md5($7), correo=$8, estado=$9
              WHERE id=$1', [$id, $nombre, $apellido, $direccion, $fecha_nacimiento, $usuario, $contrasena, $correo, $estado]);
        }
        else
        {
          $this->connection->runStatement('UPDATE biblioteca.usuario SET nombre=$2, apellido=$3, direccion=$4, fecha_nacimiento=$5, usuario=$6, correo=$7, estado=$8
              WHERE id=$1', [$id, $nombre, $apellido, $direccion, $fecha_nacimiento, $usuario, $correo, $estado]);
        }
      } catch (PDOException $e) {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function delete($id)
    {
      try
      {
        $this->connection->runStatement('UPDATE biblioteca.usuario SET estado = $2 WHERE id = $1', [$id, 'false']);
      } catch (PDOException $e) {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }
  }
}
?>

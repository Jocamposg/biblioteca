<?php
namespace Models {

  class Client
  {
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function find($id)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.cliente WHERE id = $1', [$id])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function all()
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.cliente ORDER BY id ASC');
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function create($nombre, $cedula, $telefono)
    {
      try
      {
        $this->connection->runStatement('INSERT INTO biblioteca.cliente(nombre, cedula, telefono) VALUES ($1, $2, $3)', [$nombre, $cedula, $telefono]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function update($id, $nombre, $cedula, $telefono)
    {
      try
      {
        $this->connection->runStatement('UPDATE biblioteca.cliente SET nombre=$2, cedula=$3, telefono=$4
            WHERE id=$1', [$id, $nombre, $cedula, $telefono]);
      } catch (PDOException $e) {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }
  }
}
?>

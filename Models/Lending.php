<?php
namespace Models
{
  class Lending
  {
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function find($id)
    {
      try
      {
        return $this->connection->runQuery('SELECT l.titulo, l.motivo, c.nombre, p.fecha_salida, p.fecha_entrega FROM biblioteca.libro l INNER JOIN biblioteca.prestamo p ON p.id_libro = l.id INNER JOIN biblioteca.cliente c ON c.id = p.id_cliente WHERE l.id = $1', [$id])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function name($id)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE codigo = $1', [$id])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function all()
    {
      try
      {
        return $this->connection->runQuery('SELECT l.id, l.titulo, l.codigo, l.motivo, l.tipo, c.nombre, p.fecha_salida, p.fecha_entrega FROM biblioteca.libro l INNER JOIN biblioteca.prestamo p ON p.id_libro = l.id INNER JOIN biblioteca.cliente c ON c.id = p.id_cliente ORDER BY id ASC');
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function create($libro, $cliente, $fecha_salida, $fecha_entrega)
    {
      try
      {
        $this->connection->runStatement('INSERT INTO biblioteca.prestamo(id_libro, id_cliente, fecha_salida, fecha_entrega) VALUES ($1, $2, $3, $4)', [$libro, $cliente, $fecha_salida, $fecha_entrega]);
      }
      catch (PDOException $e) {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function findDate($fecha)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE cant_prestamo >= $1 OR cant_prestamo <= $2', [$numm, $numM])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function updateTipo($codigo, $tipo)
    {
      try
      {
        $this->connection->runStatement('UPDATE biblioteca.libro SET tipo = $2 WHERE codigo = $1', [$codigo, $tipo]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function entrega($codigo, $tipo, $motivo)
    {
      print_r($codigo . $tipo . $motivo);
      try
      {
        if($motivo)
        {
          $this->connection->runStatement('UPDATE biblioteca.libro SET tipo = $2 and motivo = $3 WHERE codigo = $1', [$codigo, $tipo, ""]);
        }
        else
        {
          $this->connection->runStatement('UPDATE biblioteca.libro SET tipo = $2 WHERE codigo = $1', [$codigo, $tipo]);
        }
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

  }
}
?>
<?php
namespace Models {

  class Book
  {
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function find($id)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE id = $1', [$id])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function findTittle($titulo)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE titulo = $1', [$titulo])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function findCode($codigo)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE codigo = $1', [$codigo])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function findNum($numm, $numM)
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE cant_prestamo >= $1 OR cant_prestamo <= $2', [$numm, $numM])[0];
      }
      catch(PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function all()
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro ORDER BY id ASC');
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function allActive()
    {
      try
      {
        return $this->connection->runQuery('SELECT * FROM biblioteca.libro WHERE tipo = $1 ORDER BY id ASC', ["D"]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function create($titulo, $codigo, $tipo, $estado)
    {
      try
      {
        $this->connection->runStatement('INSERT INTO biblioteca.libro(titulo, codigo, tipo, estado) VALUES ($1, $2, $3, $4)', [$titulo, $codigo, $tipo, $estado]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function updatePrestamo($id, $cantidad)
    {
      try
      {
        $this->connection->runStatement('UPDATE biblioteca.libro SET cant_prestamo = $2 WHERE id = $1', [$id, $cantidad]);
      }
      catch (PDOException $e)
      {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function update($id, $titulo, $codigo, $tipo, $estado)
    {
      try
      {
        $this->connection->runStatement('UPDATE biblioteca.libro SET titulo=$2, codigo=$3, tipo=$4, estado=$5
            WHERE id=$1', [$id, $titulo, $codigo, $tipo, $estado]);
      } catch (PDOException $e) {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }

    public function delete($id)
    {
      try
      {
        $this->connection->runStatement('UPDATE biblioteca.libro SET estado = $2 WHERE id = $1', [$id, 'false']);
      } catch (PDOException $e) {
        echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
      }
    }
  }
}
?>

<?php
require_once __DIR__ . '/../Db/PgConnection.php';
require_once __DIR__ . '/../Models/User.php';
require_once __DIR__ . '/../Models/Client.php';
require_once __DIR__ . '/../Models/Book.php';
require_once __DIR__ . '/../Models/Lending.php';

use Db\PgConnection;

try
{
	$con = new PgConnection('postgres', '123', 'UNIVERSIDAD', 5432, 'localhost');
	//$con = new PgConnection('postgres', '12345', 'UNIVERSIDAD', 5432, 'localhost');

	$con->connect();

	$user_model = new Models\User($con);
	$client_model = new Models\Client($con);
	$book_model = new Models\Book($con);
	$lending_model = new Models\Lending($con);
}
catch (PDOException $e)
{
	echo "<script type='text/javascript'>alert('$e->getMessage');</script>";
}
?>

<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #2e2e2e;">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="navbar-brand" style="color: #f5efef;" href="<?=$urlInicio?>">Inicio<span class="sr-only">(current)</span></a>
      </li>
      <?php
        if(isset($_SESSION['user_id']))
        {
      ?>
      <li class="nav-item">
        <a style="color: #f5efef;" class="nav-link" href="../users/">Usuarios</a>
      </li>
      <li class="nav-item">
        <a style="color: #f5efef;" class="nav-link" href="../clients/">Clientes</a>
      </li>
      <li class="nav-item">
        <a style="color: #f5efef;" class="nav-link" href="../books/">Libros</a>
      </li>
      <li class="nav-item">
        <a style="color: #f5efef;" class="nav-link" href="../search/">Buscar</a>
      </li>
      <!--<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" style="color: #f5efef;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Buscar
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="./lending/index.php">Prestamo por libro</a>
          <a class="dropdown-item" href="../Reports/rep_det.php">Detalle</a>
          <a class="dropdown-item" href="../reports/rep_factu.php">Facturación</a>
      </li>-->
      <?php

        }
      ?>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <a class="nav-link" href="<?=$validador?>"><?=$estado?></a>
    </form>
  </div>
</nav>

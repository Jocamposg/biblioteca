<?php
$title = 'Bliblioteca - Estadistica';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../shared/db.php';

$urlInicio = '../';
$validador = '../validador.php';

if(!isset($_SESSION['estado']) || empty($_SESSION['estado']))
  {
    $estado = 'Sign in';
    require_once '../shared/Navbar.php';
  }
  else
  {
    $estado = 'Sign out';
    require_once '../shared/Navbar.php';
  }

  $id = $_GET['id'] ?? 0;

  $row = $lending_model->find($id);
?>

<br><br><br>
<div class="row">
  <div class="col-md-1">

  </div>
  <div class="col-md-10">
    <div class="container">
      <h1 class="text-center">Historial prestamo del libro <?=$row['tutulo']?></h1>
    </div>

    <div style="margin: 10px;" class="card card-body rounded">
      <div class="table-responsive">
        <table id="tablePreview" class="table table-striped table-sm table-bordered">
          <thead>
            <tr class="table-dark">
              <th class="text-center">Codigo</th>
              <th class="text-center">Cliente</th>
              <th class="text-center">Fecha prestamo</th>
              <th class="text-center">Fecha devolución</th>
            </tr>
          </thead>

          <tbody>
            <?php
              if($row)
              {
            ?>
                <tr>
                  <td><?=$row['codigo']?></td>
                  <td><?=$row['nombre']?></td>
                  <td><?=$row['fecha_salida']?></td>
                  <td><?=$row['fecha_entrega']?></td>
                  <td class="text-center">
                    <a class="btn btn-warning" href="/clients/update.php?id=<?=$client['id']?>">Editar</a>
                  </td>
                </tr>
            <?php
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-1">

  </div>
</div>
<?php require_once '../shared/footer.php' ?>

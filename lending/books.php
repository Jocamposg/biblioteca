<?php
$title = 'Biblioteca - Prestamos';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../shared/db.php';

$urlInicio = '../';
$validador = '../validador.php';

if(!isset($_SESSION['estado']) || empty($_SESSION['estado']))
{
  $estado = 'Sign in';
  require_once '../shared/Navbar.php';
}
else
{
  $estado = 'Sign out';
  require_once '../shared/Navbar.php';
}
$id = $_POST['id'] ?? '';
$tipo = $_POST['tipo'] ?? '';

date_default_timezone_set('America/Costa_Rica');

$fecha = date('Y-m-d');

$rows = $lending_model->all();

foreach ($rows as $row) {
	if(($row['fecha_entrega'] < $fecha) && ($row['tipo'] != 'D' && $row['tipo'] != 'A'))
	{
		$lending_model->updateTipo($row['codigo'], "N");
	}
}

?>

<br><br>
<div class="row">
  <div class="col-md-1">
  </div>
  <div class="col-md-10">
    <div class="container">
      <h1 class="text-center">Control de libros</h1>
    </div>
    <div style="margin: 10px;" class="card card-body rounded">
      <div class="table-responsive">
        <table id="tablePreview" class="table table-striped table-sm table-bordered">
          <thead>
            <tr class="table-dark">
              <th class="text-center">Id</th>
              <th class="text-center">Titulo</th>
              <th class="text-center">Codigo</th>
              <th class="text-center">Usuario</th>
              <th class="text-center">Estado</th>
              <th class="text-center">Motivo</th>
              <th class="text-center"></th>
            </tr>
          </thead>

          <tbody>
            <?php
            $books = $lending_model->all();
            if($books)
            {
              foreach ($books as $book) 
              {
              	if($book['tipo'] != "D")
              	{
              		require './row.php';
              	}  
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-1">
  </div>
</div>

<?php require_once '../shared/footer.php'?>
<div class="container">
<div  class = "row justify-content-left " >
  <div  class = "col-md-offset-5 col-md-7" >
    <form method="POST">
      <div class="form-group">
        <label>Titulo:</label>
        <select name="libro" class="form-control" autofocus>
          <?php
            foreach ($books as $book)
            {
          ?>
          <option value="<?=$book['id']?>"><?=$book['titulo']?></option>
          <?php
            }
          ?>
        </select>
        <label>Cliente:</label>
        <select name="cliente" class="form-control">
          <?php
            foreach ($clients as $client)
            {
          ?>
          <option value="<?=$client['id']?>"><?=$client['nombre']?></option>
          <?php
            }
          ?>
        </select>
        <label>Fecha prestamo:</label>
        <input type="date" name="fechaprestamo" class="form-control" value="<?=$fechaprestamo?>" required>
        <label>Fecha devolucion:</label>
        <input type="date" name="fechadevolucion" class="form-control" value="<?=$fechadevolucion?>" required>
      </div>
      <input class="btn btn-primary" type="submit" value="Aceptar">
      <a href="/books" class="btn btn-danger">Cancelar</a>
    </form>
  </div>
</div>
</div>

<?php
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';

$id = $_GET['id'] ?? 0;

$book = $lending_model->name($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  if($tipo == 'P' || $tipo == 'N')
		{
			$lending_model->entrega($id, 'D', '1');
		}
		else
		{
			$lending_model->entrega($id, 'D', '');
	}
  return header('Location: /lending/books.php');
}
?>

<div class="container">
  <p>
  	Desea cambiar el estado del libro <?=$book['titulo']?>?
  </p>
  <form method="POST">
  	<input class="btn btn-primary" type="submit" value="Aceptar">
  	<a href="/lending/books.php" class="btn btn-danger">Cancelar</a>
  </form>
</div>

<?php require_once '../shared/footer.php' ?>
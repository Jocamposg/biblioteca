<?php
	$title = 'Biblioteca - Registar prestamo';
	require_once '../shared/header.php';
	require_once '../shared/sessions.php';
	require_once '../shared/guard.php';
	require_once '../shared/db.php';

	$libro = $_POST['libro'] ?? '';
	$cliente = $_POST['cliente'] ?? '';
	$fechaprestamo = $_POST['fechaprestamo'] ?? '';
	$fechadevolucion = $_POST['fechadevolucion'] ?? '';

	$books = $book_model->allActive();
	$clients = $client_model->all();

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$cant = 0;
		$tipo = '';
		foreach ($books as $book) {
			if($book['id'] == $libro)
			{
				$cant = $book['cant_prestamo'] + 1;
				$tipo = 'P';
			}
		}

		$book_model->updatePrestamo($libro, $cant, $tipo);
		$lending_model->create($libro, $cliente, $fechaprestamo, $fechadevolucion);
  	return header('Location: ../');
	}
?>

<div class="container">
  <h1><?=$title?></h1>
  <?php require_once './form.php'?>
</div>

<?php require_once '../shared/footer.php' ?>

<?php
require_once './shared/sessions.php';

	if(!isset($_SESSION['estado']) || empty($_SESSION['estado']))
	{
		return header('Location: ./login.php');
	}
	else
	{
		require_once './logout.php';
	}
?>

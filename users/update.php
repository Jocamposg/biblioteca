<?php
$title = 'Editar usuario';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';

$id = $_GET['id'] ?? 0;
$nombre = $_POST['nombre'] ?? '';
$apellido = $_POST['apellido'] ?? '';
$direccion = $_POST['direccion'] ?? '';
$fecha_nacimiento = $_POST['fecha_nacimiento'] ?? '';
$usuario = $_POST['usuario'] ?? '';
$contrasena = $_POST['contrasena'] ?? '';
$contrasena2 = $_POST['contrasena2'] ?? '';
$correo = $_POST['correo'] ?? '';
$estado = $_POST['estado'] ?? '';

$row = $user_model->find($id);

$user = ['id' => $row['id'],'nombre' => $row['nombre'], 'apellido' => $row['apellido'], 'direccion' => $row['direccion'], 'fecha_nacimiento' =>$row['fecha_nacimiento'], 'usuario' => $row['usuario'], 'contrasena' => $row['contrasena'], 'contrasena2' => $row['contrasena'], 'correo' => $row['correo'], 'estado' => $row['estado']];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	if($contrasena == $contrasena2)
	{  
    if ($contrasena != '') {
      if($estado == "on")
      {
        $estado = 'true';
      }
      else
      {
        $estado = 'false';
      }
      $user_model->update($id, $nombre, $apellido, $direccion, $fecha_nacimiento, $usuario, $contrasena, $correo, $estado);
      return header('Location: /users');
    }
    else
    {
      print_r('pasó sin');
      if($estado == "on")
      {
        $estado = 'true';
      }
      else
      {
        $estado = 'false';
      }
      $user_model->update($id, $nombre, $apellido, $direccion, $fecha_nacimiento, $usuario, $contrasena, $correo, $estado);
      return header('Location: /users');
    }
  }
  else
  {
  	echo  "<script type='text/javascript'>alert('Las contraseñas son diferentes.');</script>";
  }
}
?>

<div class="container">
    <h1><?=$title?></h1>

    <?php require_once __DIR__ . '/form.php' ?>
</div>

<?php require_once '../shared/footer.php' ?>
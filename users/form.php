<div  class = "row justify-content-left " >
  <div  class = "col-md-offset-5 col-md-7" >
    <form method="POST">
      <div class="form-group">
        <label>Nombre(*):</label>
        <input type="text" name="nombre" class="form-control" required autofocus="true" value="<?=$user['nombre']?>">
        <label>Apellidos(*):</label>
        <input type="text" name="apellido" class="form-control" required value="<?=$user['apellido']?>">
        <label>Dirección:</label>
        <input type="text" name="direccion" class="form-control" value="<?=$user['direccion']?>">
        <label>Fecha nacimiento:</label>
        <input type="date" name="fecha_nacimiento" class="form-control" value="<?=$user['fecha_nacimiento']?>">
        <label>Usuario:</label>
        <input type="text" name="usuario" class="form-control" value="<?=$user['usuario']?>">
        <label>Contraseña(*):</label>
        <input type="password" name="contrasena" class="form-control" value="">
        <label>Confirmar contraseña(*):</label>
        <input type="password" name="contrasena2" class="form-control" value="">
        <label>Correo:</label>
        <input type="email" name="correo" class="form-control" value="<?=$user['correo']?>" required>
        <label>Estado:</label>
        <?php
        if(($user['estado'] == 't') || ($user['estado'] == null))
        {
          ?>
          <input type="checkbox" name="estado" checked>
          <?php
        }
        else
        {
          ?>
          <input type="checkbox" name="estado">
          <?php
        }
        ?>
        <br>
        <small id="emailHelp" class="form-text text-muted">(*)Campo Obligatorio</small>
      </div>
      <input class="btn btn-primary" type="submit" value="Aceptar">
      <a href="/users" class="btn btn-danger">Cancelar</a>
    </form>
  </div>
</div>

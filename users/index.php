<?php
$title = 'Biblioteca - Usuarios';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../shared/db.php';

$urlInicio = '../';
$validador = '../validador.php';

if(!isset($_SESSION['estado']) || empty($_SESSION['estado']))
  {
    $estado = 'Sign in';
    require_once '../shared/Navbar.php';
  }
  else
  {
    $estado = 'Sign out';
    require_once '../shared/Navbar.php';
  }
?>

<br><br><br>
<div class="row">
  <div class="col-md-1">
    
  </div>
  <div class="col-md-10">
    <div class="container">
      <h1 class="text-center">Control de usuarios</h1>
    </div>

    <div style="margin: 10px;" class="card card-body rounded">
      <div class="table-responsive">
        <table id="tablePreview" class="table table-striped table-sm table-bordered">
          <thead>
            <tr class="table-dark">
              <th class="text-center">Id</th>
              <th class="text-center">Nombre</th>
              <th class="text-center">Apellidos</th>
              <th class="text-center">Dirección</th>
              <th class="text-center">Fecha nacimiento</th>
              <th class="text-center">Usuario</th>
              <th class="text-center">Correo</th>
              <th class="text-center">Estado</th>
              <th class="text-center">
                <a class="btn btn-success" href="/users/create.php">Agregar</a>
              </th>
            </tr>
          </thead>

          <tbody>
            <?php
              $users = $user_model->all();
              if($users)
              {
                foreach ($users as $user) {
                  require './row.php';
                }
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-1">
    
  </div>
</div>
<?php require_once '../shared/footer.php' ?>

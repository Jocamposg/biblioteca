<?php
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';

$id = $_GET['id'] ?? 0;

$user = $user_model->find($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $user_model->delete($id);
    return header('Location: /users');
}

?>

<div class="container">
    <p>
    	Desea eliminar el usuario <?=$user['usuario']?>?
    </p>
    <form method="POST">
    <input class="btn btn-primary" type="submit" value="Aceptar">
    <a href="/users" class="btn btn-danger">Cancelar</a>
    </form>
</div>

<?php require_once '../shared/footer.php' ?>

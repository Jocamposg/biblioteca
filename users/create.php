<?php
	$title = 'Nuevo usuario';
	require_once '../shared/header.php';
	require_once '../shared/sessions.php';
	require_once '../shared/guard.php';

	$nombre = $_POST['nombre'] ?? '';
	$apellido = $_POST['apellido'] ?? '';
	$direccion = $_POST['direccion'] ?? '';
	$fecha_nacimiento = $_POST['fecha_nacimiento'] ?? '';
	$usuario = $_POST['usuario'] ?? '';
	$contrasena = $_POST['contrasena'] ?? '';
	$contrasena2 = $_POST['contrasena2'] ?? '';
	$correo = $_POST['correo'] ?? '';
	$estado = $_POST['estado'] ?? '';

	$user = ['nombre' => $nombre, 'apellido' => $apellido, 'direccion' => $direccion, 'fecha_nacimiento' => $fecha_nacimiento, 'usuario' => $usuario, 'contrasena' => '', 'contrasena2' => '', 'correo' => $correo, 'estado' => $estado];

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if($contrasena == $contrasena2)
		{
			require_once '../shared/db.php';

			$user_model->create($nombre, $apellido, $direccion, $fecha_nacimiento, $usuario, $contrasena, $correo, $estado);
    	return header('Location: /users');
  	}
  	else
  	{
  		echo  "<script type='text/javascript'>alert('Las contraseñas son diferentes.');</script>";
  	}
	}
?>

<div class="container">
  <h1><?=$title?></h1>
  <?php require_once './form.php'?>
</div>

<?php require_once '../shared/footer.php' ?>

<tr>
	<th class="table-dark"><?=$user['id']?></th>
	<td><?=$user['nombre']?></td>
	<td><?=$user['apellido']?></td>
	<td><?=$user['direccion']?></td>
	<td><?=$user['fecha_nacimiento']?></td>
	<td><?=$user['usuario']?></td>
	<td><?=$user['correo']?></td>

	<?php
		if($user['estado'] == 't')
		{
			echo "<td>";
			echo "Activo";
			echo "</td>";
		}
		else
		{
			echo "<td>";
			echo "Inactivo";
			echo "</td>";
		}
	?>
	<td class="text-center">
		<a class="btn btn-warning" href="/users/update.php?id=<?=$user['id']?>">Editar</a>
		<?php
			if($user['estado'] == 't')
			{
		?>
		<a class="btn btn-danger" href="/users/delete.php?id=<?=$user['id']?>">Eliminar</a>
		<?php
			}
		?>
	</td>
</tr>

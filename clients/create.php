<?php
	$title = 'Biblioteca - Nuevo cliente';
	require_once '../shared/header.php';
	require_once '../shared/sessions.php';
	require_once '../shared/guard.php';

	$nombre = $_POST['nombre'] ?? '';
	$cedula = $_POST['cedula'] ?? '';
	$telefono = $_POST['telefono'] ?? '';

	$client = ['nombre' => $nombre, 'cedula' => $cedula, 'telefono' => $telefono];

	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		require_once '../shared/db.php';

		$client_model->create($nombre, $cedula, $telefono);
  	return header('Location: /clients');
  }
?>

<div class="container">
  <h1><?=$title?></h1>
  <?php require_once './form.php'?>
</div>

<?php require_once '../shared/footer.php' ?>

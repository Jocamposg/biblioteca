<div  class = "row justify-content-left " >
  <div  class = "col-md-offset-5 col-md-7" >
    <form method="POST">
      <div class="form-group">
        <label>Nombre(*):</label>
        <input type="text" name="nombre" class="form-control" required autofocus="true" value="<?=$client['nombre']?>">
        <label>Cedula(*):</label>
        <input type="text" name="cedula" class="form-control" required value="<?=$client['cedula']?>">
        <label>Telefono:</label>
        <input type="text" name="telefono" class="form-control" value="<?=$client['telefono']?>" required>
        <small id="emailHelp" class="form-text text-muted">(*)Campo Obligatorio</small>
      </div>
      <input class="btn btn-primary" type="submit" value="Aceptar">
      <a href="/clients" class="btn btn-danger">Cancelar</a>
    </form>
  </div>
</div>

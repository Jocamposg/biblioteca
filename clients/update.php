<?php
$title = 'Biblioteca - Editar usuario';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';

$id = $_GET['id'] ?? 0;
$nombre = $_POST['nombre'] ?? '';
$cedula = $_POST['cedula'] ?? '';
$telefono = $_POST['telefono'] ?? '';

$row = $client_model->find($id);

$client = ['id' => $row['id'],'nombre' => $row['nombre'], 'cedula' => $row['cedula'], 'telefono' => $row['telefono']];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $client_model->update($id, $nombre, $cedula, $telefono);
  return header('Location: /clients');
}
?>

<div class="container">
    <h1><?=$title?></h1>

    <?php require_once __DIR__ . '/form.php' ?>
</div>

<?php require_once '../shared/footer.php' ?>

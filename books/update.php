<?php
$title = 'Editar libro';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';

$id = $_GET['id'] ?? 0;
$titulo = $_POST['titulo'] ?? '';
$codigo = $_POST['codigo'] ?? '';
$tipo = $_POST['tipo'] ?? '';
$estado = $_POST['estado'] ?? '';

$row = $book_model->find($id);

$book = ['id' => $row['id'],'titulo' => $row['titulo'], 'codigo' => $row['codigo'], 'tipo' => $row['tipo'], 'estado' =>$row['estado']];

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
	if($estado == "on")
	{
		$estado = 'true';
	}
	else
	{
		$estado = 'false';
	}
		
  $book_model->update($id, $titulo, $codigo, $tipo, $estado);
  return header('Location: /books');
}
?>

<div class="container">
    <h1><?=$title?></h1>

    <?php require_once __DIR__ . '/form.php' ?>
</div>

<?php require_once '../shared/footer.php' ?>
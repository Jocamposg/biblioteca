<tr>
	<th class="table-dark"><?=$book['id']?></th>
	<td><?=$book['titulo']?></td>
	<td><?=$book['codigo']?></td>

	<?php
		if($book['tipo'] == 'D')
		{
			echo "<td>";
			echo "Disponible";
			echo "</td>";
		}
		else if($book['tipo'] == 'P')
		{
			echo "<td>";
			echo "Prestado";
			echo "</td>";
		}
		else if($book['tipo'] == 'A')
		{
			echo "<td>";
			echo "Archivado";
			echo "</td>";
		}
		else
		{
			echo "<td>";
			echo "No entregado";
			echo "</td>";
		}
		?>


	<?php
		if($book['estado'] == 't')
		{
			echo "<td>";
			echo "Activo";
			echo "</td>";
		}
		else
		{
			echo "<td>";
			echo "Inactivo";
			echo "</td>";
		}
	?>
	<td class="text-center">
		<a class="btn btn-warning" href="/books/update.php?id=<?=$book['id']?>">Editar</a><br>
		<?php
			if($book['estado'] == 't')
			{
		?>
		<a class="btn btn-danger" href="/books/delete.php?id=<?=$book['id']?>">Archivar</a>
		<?php
			}
		?>
	</td>
</tr>

<?php
	$title = 'Nuevo libro';
	require_once '../shared/header.php';
	require_once '../shared/sessions.php';
	require_once '../shared/guard.php';

	$titulo = $_POST['titulo'] ?? '';
	$codigo = $_POST['codigo'] ?? '';
	$tipo = $_POST['tipo'] ?? '';
	$estado = $_POST['estado'] ?? '';

	$book = ['titulo' => $titulo, 'codigo' => $codigo, 'tipo' => $tipo, 'estado' => $estado];

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		require_once '../shared/db.php';

		if($estado == "on")
		{
			$estado = 'true';
		}
		else
		{
			$estado = 'false';
		}
		$book_model->create($titulo, $codigo, $tipo, $estado);
  	return header('Location: /books');

	}
?>

<div class="container">
  <h1><?=$title?></h1>
  <?php require_once './form.php'?>
</div>

<?php require_once '../shared/footer.php' ?>

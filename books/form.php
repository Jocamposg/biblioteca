<div  class = "row justify-content-left " >
  <div  class = "col-md-offset-5 col-md-7" >
    <form method="POST">
      <div class="form-group">
        <label>Titulo(*):</label>
        <input type="text" name="titulo" class="form-control" required autofocus="true" value="<?=$book['titulo']?>">
        <label>Código(*):</label>
        <input type="text" name="codigo" class="form-control" required value="<?=$book['codigo']?>">
        <label>Estado:</label>
        <select class="form-control" name="tipo">
          <?php
            if($book['tipo'] == 'D')
            {
          ?>
          <option value="D" selected>Disponible</option>
          <?php
            }
            else
            {
          ?>
          <option value="D">Disponible</option>
          <?php
            }
          ?>

          <?php
            if($book['tipo'] == 'P')
            {
          ?>
          <option value="P" selected>Prestado</option>
          <?php
            }
            else
            {
          ?>
          <option value="P">Prestado</option>
          <?php
            }
          ?>

          <?php
            if($book['tipo'] == 'A')
            {
          ?>
          <option value="A" selected>Archivado</option>
          <?php
            }
            else
            {
          ?>
          <option value="A">Archivado</option>
          <?php
            }
          ?>
          <?php
            if($book['tipo'] == 'N')
            {
          ?>
          <option value="A" selected>No entregado</option>
          <?php
            }
            else
            {
          ?>
          <option value="N">No entregado</option>
          <?php
            }
          ?>
        </select>
        <label>Estado:</label>
        <?php
        if(($book['estado'] == 't') || ($book['estado'] == null))
        {
          ?>
          <input type="checkbox" name="estado" checked>
          <?php
        }
        else
        {
          ?>
          <input type="checkbox" name="estado">
          <?php
        }
        ?>
        <br>
        <small id="emailHelp" class="form-text text-muted">(*)Campo Obligatorio</small>
      </div>
      <input class="btn btn-primary" type="submit" value="Aceptar">
      <a href="/books" class="btn btn-danger">Cancelar</a>
    </form>
  </div>
</div>

<?php
$title = 'Biblioteca - Libros';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../shared/db.php';

$urlInicio = '../';
$validador = '../validador.php';

if(!isset($_SESSION['estado']) || empty($_SESSION['estado']))
{
  $estado = 'Sign in';
  require_once '../shared/Navbar.php';
}
else
{
  $estado = 'Sign out';
  require_once '../shared/Navbar.php';
}
?>

<br><br>
<div class="row">
  <div class="col-md-1">

  </div>
  <div class="col-md-10">
    <div class="container">
      <h1 class="text-center">Control de libros</h1>
    </div>

    <div style="margin: 5px;" class="card card-body rounded">
      <div class="table-responsive">
        <table id="tablePreview" class="table table-striped table-sm table-bordered">
          <thead>
            <tr class="table-dark">
              <th class="text-center">Id</th>
              <th class="text-center">Titulo</th>
              <th class="text-center">Codigo</th>
              <th class="text-center">Tipo</th>
              <th class="text-center">Estado</th>
              <th class="text-center">
                <a class="btn btn-success" href="/books/create.php">Agregar</a>
              </th>
            </tr>
          </thead>

          <tbody>
            <?php
            $books = $book_model->all();
            if($books)
            {
              foreach ($books as $book) {
                require './row.php';
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-1">

  </div>
</div>
<?php require_once '../shared/footer.php' ?>

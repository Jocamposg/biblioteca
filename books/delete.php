<?php
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../shared/guard.php';

$urlInicio = '../';
$validador = '../validador.php';
$id = $_GET['id'] ?? 0;
$motivo = $_POST['motivo'] ?? '';

$book = $book_model->find($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $book_model->delete($id, $motivo);
    return header('Location: /books');
}

?>

<div class="container">
    <form method="POST">
    <p>
      Favor ingresar motivo del porque se archiva el libro:
    </p>
    <input type="text" name="motivo" class="form-control" autofocus required value="<?=$book['motivo']?>">
    <p>
    	Desea archivar el libro <?=$book['titulo']?>?
    </p>
    <input class="btn btn-primary" type="submit" value="Aceptar">
    <a href="/books" class="btn btn-danger">Cancelar</a>
    </form>
</div>

<?php require_once '../shared/footer.php' ?>

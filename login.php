<?php
require_once './shared/sessions.php';
require_once './shared/db.php';


$id = $_GET['id'] ?? 0;
$username = $_POST['username'] ?? '';
$password = $_POST['password'] ?? '';

$user = $user_model->login($username, $password);

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
  if ($user != null) {
    if($user['estado'] != 'f')
    {
      $_SESSION['user_id'] = $user['id'];
      $_SESSION['estado'] = $user['estado'];
      if($id == 0)
      {
        return header('Location: ../index.php');
      }
      else
      {
        return header("Location: ./tender.php?id=$id");
      }
    }
    else
    {
      echo "<script type='text/javascript'>alert('El usuario se encuentra desactivado.');</script>";
    }
  }
  else
  {
    echo "<script type='text/javascript'>alert('El usuario y/o contraseña son incorrectos.');</script>";
  }
}

require_once './shared/header.php';
?>
<br><br><br><br><br>
<form method="POST">
  <div class="container" id="formContainer">
    <div class="row justify-content-center">
      <div class="col-md-offset-5 col-md-3 rounded float-left border border-secondary" style="background-color: #f0eeee;">
        <h3 class="text-center">Ingreso</h3>
        <br>
        <label>Usuario:</label>
        <input autofocus="true" type="text" class="form-control" name="username" value="<?=$username?>" placeholder="" required>
        <br>
        <label>Contrasena:</label>
        <input type="password" class="form-control" name="password" placeholder="" required>
        <br>
        <a href="../index.php" class="btn btn-danger btn-block">Cancelar</a>
        <button class="btn btn-primary btn-block">Ingresar</button>
        <br>
      </div>
    </div>
  </div>
</form>

<?php require_once './shared/footer.php'?>

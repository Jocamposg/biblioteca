<?php
$title = 'Biblioteca - Libros';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/guard.php';
require_once '../shared/db.php';

$urlInicio = '../';
$validador = '../validador.php';
$titulo = $_POST['titulo'] ?? '';
$codigo = $_POST['codigo'] ?? '';
$estadoL = $_POST['selectE'] ?? '';
$cant_ini = $_POST['cant_ini'] ?? '';
$cant_fin = $_POST['cant_fin'] ?? '';
$fecha_ini = $_POST['fecha_ini'] ?? '';
$fecha_fin = $_POST['fecha_fin'] ?? '';


if(!isset($_SESSION['estado']) || empty($_SESSION['estado']))
{
  $estado = 'Sign in';
  require_once '../shared/Navbar.php';
}
else
{
  $estado = 'Sign out';
  require_once '../shared/Navbar.php';
}
?>

<br><br>
<div class="row">
  <div class="col-md-1">

  </div>
  <div class="col-md-10">
    <div class="container">
      <h1 class="text-center">Control de libros</h1>
    </div>

    <div style="margin: 10px;" class="card card-body rounded">
      <div  class = "row justify-content-left " >
        <div  class = "col-md-offset-5 col-md-7" >
          <form method="POST">
            <div class="form-group">
              <label>Seleccione opcion e ingrese datos</label>
              <br>
              <label>Filtrar libro por:</label>
              <div class="radio">
                <label>
                  <input type="radio" name="checkboxtitulo" id="opciones_1" value="opcion_1">
                  Titulo
                </label>
                <div class="input-group">
                  <input autofocus="true" type="text" class="form-control" name="titulo" placeholder="Inserte titulo de libro" value="">
                </div>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="checkboxcodigo" id="opciones_2" value="opcion_2">
                  Codigo
                </label>
                <div class="input-group">
                  <input type="text" class="form-control" name="codigo" placeholder="Inserte codigo de libro" value="">
                </div>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="checkboxestado" id="opciones_1" value="opcion_1">
                  Estado
                </label>
                <br>
                <div class="input-group">
                  <select name="selectE" class="custom-select" id="inputGroupSelect04">
                    <option selected>Elegir...</option>
                    <option value="P">Prestado</option>
                    <option value="D">Disponible</option>
                    <option value="A">Archivado</option>
                    <option value="N">No Entregado</option>
                  </select>
                </div>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="checkboxcantidad" id="opciones_2" value="opcion_2">
                  Cantidad de prestamos
                </label>
                <div class="input-group">
                  <label>Desde:</label>
                  <input type="text" class="form-control" name="cant_ini" placeholder="Inserte inicial" value="">
                  <label>Hasta:</label>
                  <input type="text" class="form-control" name="cant_fin" placeholder="Inserte final" value="">
                </div>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="checkboxfecha" id="opciones_2" value="opcion_2">
                  Fecha de prestamo
                </label>
                <div class="input-group">
                  <label>Desde:</label>
                  <input type="date" class="form-control" name="fecha_ini" placeholder="" value="">
                  <label>Hasta:</label>
                  <input type="date" class="form-control" name="fecha_fin" placeholder="" value="">
                  <!--<div class="input-group-append">
                  <input class="btn btn-dark" type="submit" value="Filtrar">
                </div>-->
              </div>
            </div>
            <br>
            <div class="input-group-append">
              <input class="btn btn-dark" type="submit" value="Filtrar">
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="table-responsive">
      <table id="tablePreview" class="table table-striped table-sm table-bordered">
        <thead>
          <tr class="table-dark">
            <th class="text-center">Id</th>
            <th class="text-center">Titulo</th>
            <th class="text-center">Codigo</th>
            <th class="text-center">Cantidad de prestamos</th>
            <th class="text-center">Estado</th>
            <th class="text-center">Fecha prestamos</th>
            <th class="text-center">Fecha entrega</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($_POST['checkboxtitulo'])&&!empty($_POST['titulo'])) {
            $books = $book_model->allLendT($titulo);
            if($books)
            {
              foreach ($books as $book) {
                require './row.php';
              }
            }
          }elseif (!empty($_POST['checkboxcodigo'])&&!empty($_POST['codigo'])) {
            $books = $book_model->allLendC($codigo);
            if($books)
            {
              foreach ($books as $book) {
                require './row.php';
              }
            }
          }elseif (!empty($_POST['checkboxestado'])) {
            $books = $book_model->allLendE($estadoL);
            if($books)
            {
              foreach ($books as $book) {
                require './row.php';
              }
            }
          }elseif (!empty($_POST['checkboxcantidad'])&&!empty($_POST['cant_ini'])&&!empty($_POST['cant_fin'])) {
            $books = $book_model->allLendCant($cant_ini, $cant_fin);
            if($books)
            {
              foreach ($books as $book) {
                require './row.php';
              }
            }
          }elseif (!empty($_POST['checkboxfecha'])&&!empty($_POST['fecha_ini'])&&!empty($_POST['fecha_fin'])) {
            $books = $book_model->allLendFecha($fecha_ini, $fecha_fin);
            if($books)
            {
              foreach ($books as $book) {
                require './row.php';
              }
            }
          }else {
            $books = $book_model->allLend();
            if($books)
            {
              foreach ($books as $book) {
                require './row.php';
              }
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="col-md-1">

</div>
</div>
<?php require_once '../shared/footer.php' ?>

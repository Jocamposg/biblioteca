--CREATE DATABASE UNIVERSIDAD;

CREATE SCHEMA BIBLIOTECA;

CREATE TABLE BIBLIOTECA.usuario (
	id SERIAL PRIMARY KEY,
	nombre TEXT NOT NULL,
	apellido TEXT NOT NULL,
	direccion TEXT NOT NULL,
	fecha_nacimiento DATE,
	usuario TEXT NOT NULL UNIQUE,
	contrasena TEXT NOT NULL, --Encriptado en MD5
	correo TEXT NOT NULL,
	estado BOOLEAN DEFAULT TRUE
);

CREATE TABLE BIBLIOTECA.libro (
	id SERIAL PRIMARY KEY,
	titulo TEXT NOT NULL,
	codigo TEXT NOT NULL,
	cant_prestamo INT NOT NULL DEFAULT 0,
	tipo CHAR NOT NULL DEFAULT 'D',
	estado BOOLEAN DEFAULT TRUE,
	motivo TEXT
);

CREATE TABLE BIBLIOTECA.cliente (
	id SERIAL PRIMARY KEY,
	nombre TEXT NOT NULL,
	cedula TEXT NOT NULL,
	telefono TEXT
);

CREATE TABLE BIBLIOTECA.prestamo (
	id SERIAL PRIMARY KEY,
	id_libro INT NOT NULL,
	id_cliente INT NOT NULL,
	fecha_salida DATE NOT NULL,
	fecha_entrega DATE NOT NULL
);

ALTER TABLE BIBLIOTECA.prestamo ADD CONSTRAINT fk_idlibro FOREIGN KEY(id_libro) REFERENCES BIBLIOTECA.libro(id);
ALTER TABLE BIBLIOTECA.prestamo ADD CONSTRAINT fk_idcliente FOREIGN KEY(id_cliente) REFERENCES BIBLIOTECA.cliente(id);

INSERT INTO biblioteca.usuario(nombre, apellido, direccion, fecha_nacimiento, usuario, contrasena, correo, estado)
   	VALUES ('Administrador', '', '', CURRENT_DATE, 'admin', '123', 'dcampos095@gmail.com', true);
